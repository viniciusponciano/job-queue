(ns job-queue.core
  (:require [clojure.data.json :as json]
            [clojure.string :as str]))

; Global unbound variables
(def agents)
(def jobs)
(def job-requests)
(def user-input)

; Print anything passed as param
(defn print-something
  [x]
  (println x))

; Check if param is an agent and return a bool value
(defn is-agent?
  [element]
  (if (= (get-in element ["new_agent"]) nil) false true)
  )

; Check if param is a job and return a bool value
(defn is-job?
  [element]
  (if (= (get-in element ["new_job"]) nil) false true)
  )

; Check if param is a job request and return a bool value
(defn is-job-request?
  [element]
  (if (= (get-in element ["job_request"]) nil) false true)
  )

; Sort callback function to put urgent jobs first
(defn urgents-first
  [e1 e2]
  (if (and (false? (e1 "urgent")) (true? (e2 "urgent"))) false true)
  )

; Fetch the agent at a agent element
(defn get-agent
  [agent]
  (agent "new_agent")
  )

; Fetch the job at a job element
(defn get-job
  [agent]
  (agent "new_job")
  )

; Fetch the job request at a job request element
(defn get-job-request
  [agent]
  (agent "job_request")
  )

; Find and return the agent by id on agents collection
(defn recursive-agents
  ([agent-id] (recursive-agents 0 agent-id))
  ([i agent-id]
   (if (< i (count agents))
     (if (= agent-id ((agents i) "id"))
       (agents i)
       (recursive-agents (+ i 1) agent-id)
       )
     nil
     )
    )
  )

; Find and return the job by type on jobs collection
(defn recursive-jobs
  ([skill output] (recursive-jobs 0 skill output))
  ([i skill output]
   (if (< i (count jobs))
     (if (and (= skill ((jobs i) "type")) (not (str/includes? output ((jobs i) "id"))))
       (jobs i)
       (recursive-jobs (+ i 1) skill output)
       )
     nil
     )
    )
  )

; Relate the agent skills with a best fit job
(defn recursive-skills
  ([skills output] (recursive-skills 0 skills output))
  ([i skills output]
   (if (< i (count skills))
     (if (nil? (recursive-jobs (skills i) output))
       (recursive-skills (+ i 1) skills output)
       (recursive-jobs (skills i) output)
       )
     nil
     )
    )
  )

; Build the output string JSON, according job request matching agents and jobs
(defn assign-jobs
  ([] (assign-jobs 0 ""))
  ([i output]
   (if (< i (count job-requests))
     (assign-jobs
       (+ i 1)
       (if (and
             (or
               (not
                 (nil? (recursive-skills ((recursive-agents ((job-requests i) "agent_id")) "primary_skillset") output))
                 )
               (not
                 (nil? (recursive-skills ((recursive-agents ((job-requests i) "agent_id")) "secondary_skillset") output))
                 )
               )
             (not (nil? (recursive-agents ((job-requests i) "agent_id"))))
             )
         (str
           output
           "{ \"job_assigned\": {\"job_id\": \""
           (if (not (nil? (recursive-skills ((recursive-agents ((job-requests i) "agent_id")) "primary_skillset") output)))
             ((recursive-skills ((recursive-agents ((job-requests i) "agent_id")) "primary_skillset") output) "id")
             ((recursive-skills ((recursive-agents ((job-requests i) "agent_id")) "secondary_skillset") output) "id")
             )
           "\", \"agent_id\": \""
           ((recursive-agents ((job-requests i) "agent_id")) "id")
           "\"} }, "
           )
         output
         )
       )
     (str "[" (subs output 0 (- (count output) 2)) "]")
     )
    )
  )

; Read the input string JSON to use on Clojure
(defn make-conj
  []
  (def user-input (json/read-str (slurp "input.txt")))
  )

; Assign the variables with the properly collection
(defn define-elements
  []
  (def agents (mapv get-agent (filter is-agent? user-input)))
  (def jobs (mapv get-job (sort urgents-first (filter is-job? user-input))))
  (def job-requests (mapv get-job-request (filter is-job-request? user-input)))
  )

; Main function that start the application
(defn -main
  "Read from STDIN"
  [& args]
  (try
    (println "Enter input JSON:")
    (make-conj)
    (define-elements)
    (println "Output JSON with assignments:")
    (spit "output.txt" (assign-jobs))
    (print-something (assign-jobs))
    (catch Exception e (str "Wrong JSON input format: " (.getMessage e))))
  )