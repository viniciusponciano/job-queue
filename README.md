# job-queue

This project is meant to resolve a job queue similar with a problem that NuBank already solved.

It's the first part of a practical selection process to be hired by NuBank.

## Usage

The program is developed with Leiningen, so to run this with use a local REPL.

First you need to put a file named `input.txt` in root directory of project.

Than you can run the Clojure application on terminal at project folder `/src` the command `lein repl`.

After that you can import the required namespace job-queue with `(require 'job-queue.core)`.

And then you can run the program with `(job-queue.core/-main)`.

After that a file named `output.txt` will be created in root directory of project.

or

If you using cursive you can checkout on cursive branch `git checkout cursive` and then run with local REPL on IntelliJ configuration.

Extra configuration need to be set in before launch option: `Synchronize Leiningen Project` and `Build`.

After that you can call the main method with `(job-queue.core/-main)`, input a string JSON on dialog and click ok.

The output will be showed on stdout.

That's it.
## License

Copyright © 2018 - Vinícius Henrique Ponciano Ribeiro.

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
